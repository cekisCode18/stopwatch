﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stopwatch
{
    class Program
    {
        static void Main(string[] args)
        {
            bool continueLoop = true;
            var watch = new Stopwatch();

            do
            {
                Console.WriteLine("\nPress z to start timer, x to stop timer, q to Quit Stopwatch:");
                char input = Console.ReadKey().KeyChar;

                switch (input)
                {
                    case 'z':
                        {
                            watch.StartTimer();
                            break;
                        }
                    case 'x':
                        {
                            watch.StopTimer();
                            TimeSpan runningTime = watch.CalculateRunningTime();
                            Console.WriteLine("\nThe timer ran for {0}", runningTime.ToString(@"hh\:mm\:ss"));
                            break;
                        }
                    case 'q':
                        {
                            continueLoop = false;
                            break;
                        }
                } 
            } while (continueLoop);
        }
    }
}
