﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stopwatch
{
    public class Stopwatch
    {
        private DateTime _startTime;
        private DateTime _stopTime;
        private bool isRunning;

        public void StartTimer()
        {
            if (isRunning)
                throw new InvalidOperationException("Timer was already started.");

            _startTime = DateTime.Now;
            isRunning = true;
        }

        public void StopTimer()
        {
            if (!isRunning)
                throw new InvalidOperationException("Can't stop a Timer that hasn't been started.");

            _stopTime = DateTime.Now;
            isRunning = false;
        }

        public TimeSpan CalculateRunningTime()
        {
            return _stopTime - _startTime;
        }
    }
}
